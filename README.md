This is a simple test project.

Technologies, frameworks, tools & versions:

Java 1.7
Spring Framework 4.3.3 (Spring Data JPA, Spring MVC)
Hibernate 5.3.3 / JPA 2
Hibernate-validator
DB - MySQL 5.7 InnoDB / Embedded DB for testing - H2
JUnit 4.12 / Hamcrest 1.3 / Mockito 1.10.19
JavaScript + JQuery 3.1.1
Maven
Bootstrap Framework 3.3.7