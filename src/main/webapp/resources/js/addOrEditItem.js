$( document ).ready(function() {

    $('#add').click(function (e) {
        e.preventDefault();
        $.post(
            "add",
            {
                vendorCode   : $('#addOrEDitForm input[name=vendorCode]').val(),
                name         : $('#addOrEDitForm input[name=name]').val(),
                price  : $('#addOrEDitForm input[name=price]').val(),
                arrivalDate   : $('#addOrEDitForm input[name=arrivalDate]').val(),
                availability         : $('#addOrEDitForm input[name=availability]').is(':checked'),
                packageSize  : $('#addOrEDitForm input[name=packageSize]').val(),
            },
            function () {
                window.location.href = ".."
            }
        ).fail(function (response) {
            document.body.innerHTML = response.responseText;
        });
    });

    $('#save').click(function (e) {
        e.preventDefault();
        $.post(
            "../edit",
            {
                id : $('#addOrEDitForm input[name=id]').val(),
                vendorCode   : $('#addOrEDitForm input[name=vendorCode]').val(),
                name         : $('#addOrEDitForm input[name=name]').val(),
                price  : $('#addOrEDitForm input[name=price]').val(),
                arrivalDate   : $('#addOrEDitForm input[name=arrivalDate]').val(),
                availability         : $('#addOrEDitForm input[name=availability]').is(':checked'),
                packageSize  : $('#addOrEDitForm input[name=packageSize]').val(),
            },
            function () {
                window.location.href = "../.."
            }
        ).fail(function (response) {
            document.body.innerHTML = response.responseText;
        });
    });

    $('#cancel').click(function (e) {
        window.history.back();
    });
});

function setValuesToFormInputs(itemObject, e) {
    $('#addOrEDitForm input[name=id]').val(itemObject.id);
    $('#addOrEDitForm input[name=vendorCode]').val(itemObject.vendorCode);
    $('#addOrEDitForm input[name=name]').val(itemObject.name);
    $('#addOrEDitForm input[name=price]').val(itemObject.price);
    $('#addOrEDitForm input[name=arrivalDate]').val(itemObject.arrivalDate);
    //$('#addOrEDitForm input[name=availability]').val(itemObject.availability);
    $('#addOrEDitForm input[name=packageSize]').val(itemObject.packageSize);

    if(itemObject.availability == "true") {
        $('#addOrEDitForm input[name=availability]').prop('checked', itemObject.availability);
    }
    else if (itemObject.availability == "false") {
        $('#addOrEDitForm input[name=availability]').removeAttr('checked');
    }
}