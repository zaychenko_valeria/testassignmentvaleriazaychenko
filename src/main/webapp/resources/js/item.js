$( document ).ready(function() {

    $('#delete').click(function () {
        var itemIds = [];
        $('#itemsTable').find('tr').each(function () {
            var row = $(this);
            if($('input[type=checkbox]:checked', row).length == 1)
                itemIds.push(row.find('input[type=hidden]').val());
        });
        removeSelectedItems(JSON.stringify(itemIds));
    });

    $('#edit').click(function () {
        var itemIds = [];
        $('#itemsTable').find('tr').each(function () {
            var row = $(this);
            if($('input[type=checkbox]:checked', row).length == 1)
                itemIds.push(row.find('input[type=hidden]').val());
        });
        if(itemIds.length > 1)
            $.toast( 'Please select just one row to be edited' );
        else if(itemIds.length == 0) {
            window.location.href = "items/add"
        }
        else if(itemIds.length == 1) {
            window.location.href = "items/edit/" + itemIds.pop();
        }
    });

    $('#refresh').click(function () {
        location.reload();
    });
});

function removeSelectedItems ( itemIds ) {

    $.post(
        "items/deleteSelected",
        {
            idsString: itemIds
        },
        function () {
            location.reload();
        }
    ).fail( function( response ) {
        document.body.innerHTML = response.responseText;
    });
}
