<%@ include file="/jsp/base.jspf" %>
<%@ attribute name="htmlTitle" type="java.lang.String" rtexprvalue="true" required="true" %>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><c:out value="${fn:trim(htmlTitle)}" /></title>
  <link rel="stylesheet" href="<c:url value="/resources/css/external/bootstrap.min.css" />" />
  <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" />
  <script src="<c:url value="/resources/js/external/jquery-3.1.1.js" />"></script>
  <script src="<c:url value="/resources/js/external/ztoast.min.js" />"></script>
  <script src="<c:url value="/resources/js/external/bootstrap.min.js" />"></script>
  <script src="<c:url value="/resources/js/item.js" />"></script>
  <script src="<c:url value="/resources/js/addOrEditItem.js" />"></script>
</head>
<body>
  <jsp:doBody />
</body>
</html>