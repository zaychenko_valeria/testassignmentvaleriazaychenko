<%--@elvariable id="items" type="java.util.List<assignment.dto.ItemDto>"--%>
<template:main htmlTitle="Form" >
  <jsp:body>
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
      <form class="form-horizontal" role="form" id="addOrEDitForm">
        <fieldset>

          <legend>Form</legend>

          <input type="hidden" name="id">

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Vendor code</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                <input class="form-control" id="vendorCode" name="vendorCode" type="number" placeholder="Enter vendor code" required/>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Name</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                <input class="form-control" id="name" name="name" type="text" placeholder="Enter name" required/>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Price</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                <input class="form-control" id="price" name="price" type="number" placeholder="Enter price" required/>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Arrival date</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                <input class="form-control" id="arrivalDate" name="arrivalDate" type="date" placeholder="Enter date" required/>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Availability</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                 <div class="checkbox">
                   <label><input id="availability" name="availability" type="checkbox" value="" required></label>
                 </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><b>Package Size</b></label>
            <div class="col-sm-10">
              <div class="input-group">
                <input class="form-control" id="packageSize" name="packageSize" type="text" maxlength="1" placeholder="Enter package size" required/>
              </div>
            </div>
          </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="pull-right">
                  <c:if test="${item != null}">
                    <script>
                      var itemObject = {
                      id           : "${item.id}",
                      vendorCode   : "${item.vendorCode}",
                      name         : "${item.name}",
                      price        : "${item.price}",
                      arrivalDate  : "${item.arrivalDate}",
                      availability : "${item.availability}",
                      packageSize  : "${item.packageSize}"
                      };
                      setValuesToFormInputs( itemObject );
                    </script>
                    <button id="save" class="btn btn-primary">Save</button>
                  </c:if>
                  <c:if test="${item == null}">
                    <button id="add" class="btn btn-primary">Add</button>
                  </c:if>
                  <button id="cancel" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </jsp:body>
</template:main>
