<%--@elvariable id="items" type="java.util.List<assignment.dto.ItemDto>"--%>
<template:main htmlTitle="Items" >
  <jsp:body>
    <div class="container-fluid">
      <div class="row"><br></div>
      <div class="row">
        <div class="col-md-9">
          <div class="table-responsive">
            <table class="table table-hover" id="itemsTable">
              <thead>
                <tr>
                  <th></th>
                  <th>Vendor Code</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Arrival Date</th>
                  <th>Availability</th>
                  <th>Package Size</th>
              </tr>
              </thead>

              <tbody>
                <c:forEach items="${items}" var="item">
                  <tr>
                    <td>
                      <input name="selectAll" type="checkbox">
                      <input type="hidden" name="itemId" value=${item.id}>
                    </td>
                    <td>${item.vendorCode}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>${item.arrivalDate}</td>
                    <td>${item.availability}</td>
                    <td>${item.packageSize}</td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-offset-1"></div>
        <div class="col-md-3">
          <button class="btn btn-primary btn-sm btn-block" type="button" id="refresh">
            Refresh
          </button>
          <button class="btn btn-primary btn-sm btn-block" type="button" id="edit">Edit</button>
          <button class="btn btn-primary btn-sm btn-block" type="button" id="delete">Delete</button>
        </div>
        <div class="col-md-offset-1"></div>
      </div>
    </div>
  </jsp:body>
</template:main>