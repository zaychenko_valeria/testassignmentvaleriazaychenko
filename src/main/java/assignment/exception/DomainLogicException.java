package assignment.exception;

public class DomainLogicException extends RuntimeException {

    private String name;

    public DomainLogicException(String name) {
        super(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

