package assignment.exception;

public class EntityNotFoundException extends DomainLogicException {

    public EntityNotFoundException(String name) {
        super("Entity not found " + name);
    }
}
