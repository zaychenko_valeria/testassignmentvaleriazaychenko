package assignment.utils;

import assignment.domain.Item;
import assignment.dto.ItemDto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class DtoBuilder {

    public static ItemDto toDto(Item item) {
        ItemDto dto = new ItemDto();
        dto.setId(item.getId());
        dto.setName(item.getName());
        dto.setVendorCode(item.getVendorCode());
        dto.setPrice(item.getPrice());
        dto.setArrivalDate(DateParser.parseDateToString(item.getArrivalDate()));
        dto.setAvailability(item.isAvailability());
        dto.setPackageSize(item.getPackageSize());
        return dto;
    }

    public static List<ItemDto> collectionToDto(Iterator<Item> iterator) {
        List<ItemDto> dtos = new ArrayList<>();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            dtos.add(DtoBuilder.toDto(item));
        }
        return dtos;
    }
}
