package assignment.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "items")
public class Item {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", unique = true)
    private String id;
    private int vendorCode;
    private String name;
    private double price;
    private boolean availability;
    private Date arrivalDate;
    private char packageSize;
    
    public Item(int vendorCode, String name, double price, boolean availability,
                Date arrivalDate, char packageSize) {

        this.vendorCode = vendorCode;
        this.name = name;
        this.price = price;
        this.availability = availability;
        this.arrivalDate = arrivalDate;
        this.packageSize = packageSize;
    }

    protected Item() {}
    
    public String getId() {
        return id;
    }

    public int getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(int vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public char getPackageSize() {
        return packageSize;
    }

    public void setPackageSize(char packageSize) {
        this.packageSize = packageSize;
    }
}
