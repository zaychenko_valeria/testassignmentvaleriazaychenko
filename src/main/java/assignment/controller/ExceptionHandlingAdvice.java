package assignment.controller;

import assignment.exception.DomainLogicException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.text.ParseException;

@ControllerAdvice
public class ExceptionHandlingAdvice {

    @ExceptionHandler(DomainLogicException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ModelAndView handle(DomainLogicException e) {
        return getModelAndView(e.getName());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ModelAndView handle(NoHandlerFoundException e)  {
        return getModelAndView(e.getMessage());
    }

    @ExceptionHandler(ParseException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ModelAndView handle(ParseException e)  {
        return getModelAndView(e.getMessage());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ModelAndView handle (ConstraintViolationException e) {
        return getModelAndView(e.getMessage());
    }

    private ModelAndView getModelAndView(String message) {
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("message", message);
        return mav;
    }
}
