package assignment.controller;

import assignment.service.IItemService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/items")
public class ItemController {

    @Inject
    private IItemService itemService;
    private ObjectMapper objectMapper = new ObjectMapper();;

    private final String MAIN_PAGE_VIEW_NAME = "item";
    private final String ADD_EDIT_PAGE_VIEW_NAME = "addOrEditItem";
    private final String ATTR_NAME_ITEM = "item";
    private final String ATTR_NAME_ITEMS = "items";

    private final String DATE_PATTERN = "yyyy-MM-dd";

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getItemsList(Map<String, Object> model) {
        model.put(ATTR_NAME_ITEMS, itemService.findAll());
        return MAIN_PAGE_VIEW_NAME;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET )
    public String add() {
        return ADD_EDIT_PAGE_VIEW_NAME;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST )
    public ResponseEntity add(@RequestParam int vendorCode, @RequestParam String name,
                      @RequestParam double price, @RequestParam boolean availability,
                      @RequestParam @DateTimeFormat(pattern=DATE_PATTERN) Date arrivalDate,
                      @RequestParam char packageSize) {
        itemService.createItem(vendorCode, name, price, availability, arrivalDate, packageSize);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET )
    public String edit(@PathVariable String id, Map<String, Object> model) {
        model.put(ATTR_NAME_ITEM, itemService.findOne(id));
        return ADD_EDIT_PAGE_VIEW_NAME;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Object edit(@RequestParam String id, @RequestParam int vendorCode, @RequestParam String name,
                       @RequestParam double price, @RequestParam boolean availability,
                       @RequestParam @DateTimeFormat(pattern=DATE_PATTERN) Date arrivalDate,
                       @RequestParam char packageSize) {
        itemService.editItem(id, vendorCode, name, price, availability, arrivalDate, packageSize);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteSelected", method = RequestMethod.POST)
    public ResponseEntity removeSelected(@RequestParam String idsString) throws IOException {
        List<String> ids = objectMapper.readValue(idsString, new TypeReference<List<String>>() {});
        for(String id : ids)
            itemService.removeItem(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
