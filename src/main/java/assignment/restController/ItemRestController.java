package assignment.restController;

import assignment.dto.ItemDto;
import assignment.service.IItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ItemRestController {

    @Inject
    private IItemService itemService;

    @RequestMapping( value = "rest/items" , method = GET )
    public Collection< String > items()
    {
        List< String > list = new ArrayList<>();
        for ( ItemDto view : itemService.findAll() )
            list.add( view.getName() );

        return list;
    }

    @RequestMapping(value = "rest/items " , method = RequestMethod.POST)
    public ResponseEntity< Void > createGroup(@RequestBody ItemDto item )
    {
        itemService.createItem( item.getVendorCode(), item.getName(),
                item.getPrice(), item.isAvailability(),
                Date.valueOf(item.getArrivalDate()), item.getPackageSize() );
        return new ResponseEntity<>( HttpStatus.CREATED);
    }
}
