package assignment.service;

import assignment.dto.ItemDto;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Stack;

@Validated
public interface IItemService {

    String createItem(@Min(value = 0) int vendorCode, @NotEmpty String name, @Min(value = 0) double price,
                      boolean availability, @NotNull Date arrivalDate, char packageSize);

    void editItem(@NotNull String id, @Min(value = 0) int vendorCode,
                  @NotEmpty String name, @Min(value = 0) double price,
                  boolean availability, @NotNull Date arrivalDate, char packageSize);

    void removeItem(@NotNull String id);

    ItemDto findOne(@NotNull String id);

    List<ItemDto> findAll();

    Stack<String> verifyActualItems(@NotNull String id);
}
 