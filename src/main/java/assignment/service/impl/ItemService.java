package assignment.service.impl;

import assignment.domain.Item;
import assignment.dto.ItemDto;
import assignment.exception.EntityNotFoundException;
import assignment.repository.ItemRepository;
import assignment.service.IItemService;
import assignment.utils.DtoBuilder;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringStack;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Stack;

import static assignment.utils.ErrorMessages.MSG_1;

@Service
public class ItemService implements IItemService {

    @Inject
    private ItemRepository itemRepository;

    @Transactional
    @Override
    public String createItem(int vendorCode, String name, double price,
                             boolean availability, Date arrivalDate, char packageSize) {

        Item item = new Item(vendorCode, name, price, availability, arrivalDate, packageSize);
        itemRepository.save(item);
        return item.getId();
    }

    @Transactional
    @Override
    public void editItem(String id, int vendorCode, String name,
                         double price, boolean availability, Date arrivalDate, char packageSize) {

        Item item = restoreFromRepository(id);

        item.setVendorCode(vendorCode);
        item.setName(name);
        item.setPrice(price);
        item.setAvailability(availability);
        item.setArrivalDate(arrivalDate);
        item.setPackageSize(packageSize);
    }

    @Transactional
    @Override
    public void removeItem(String id) {
        Item item = restoreFromRepository(id);
        itemRepository.delete(item);
    }

    @Transactional
    @Override
    public ItemDto findOne(String id) {
        Item item = restoreFromRepository(id);
        return DtoBuilder.toDto(item);
    }

    @Transactional
    @Override
    public List<ItemDto> findAll() {
        return DtoBuilder.collectionToDto(itemRepository.findAll().iterator());
    }

    @Transactional
    @Override
    public Stack<String> verifyActualItems(String id) {
        Stack<String> errMsg = new StringStack();
        Item item = restoreFromRepository(id);
        if(!item.isAvailability()){
            item.setArrivalDate(null);
            item.setPrice(0.0);
        }
        else
            if(item.getArrivalDate() != null)
            errMsg.push(MSG_1);
        return errMsg;
    }

    private Item restoreFromRepository(String id) {
        Item item = itemRepository.findOne(id);
        if (item == null)
            throw new EntityNotFoundException(Item.class.getName());
        return item;
    }
}
