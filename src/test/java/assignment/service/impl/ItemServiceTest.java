package assignment.service.impl;

import assignment.domain.Item;
import assignment.dto.ItemDto;
import assignment.exception.EntityNotFoundException;
import assignment.repository.ItemRepository;
import assignment.service.IItemService;
import assignment.utils.DateParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = assignment.config.JUnitConfiguration.class)
@Transactional()
public class ItemServiceTest {

    @Inject
    private IItemService itemService;
    @Inject
    private ItemRepository itemRepository;

    private final int VENDOR_CODE = 20;
    private final String NAME = "clothes";
    private final double PRICE = 5.00;
    private final boolean AVAILABILITY = false;
    private final Date ARRIVAL_DATE = new GregorianCalendar(2017, 11, 11).getTime();
    private final char PACKAGE_SIZE = 'L';

    private final String RANDOM_UUID = UUID.randomUUID().toString();

    //---CREATE ITEM TESTS----------------------------------------------------------------------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void createItemNullFields() {
        itemService.createItem(0, null, 0.00, false, null, '-');
    }

    @Test(expected = ConstraintViolationException.class)
    public void createItemNegativeVendorCode() {
        itemService.createItem(-20, NAME, PRICE, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void createItemEmptyName() {
        itemService.createItem(VENDOR_CODE, "", PRICE, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void createItemNegativePrice() {
        itemService.createItem(VENDOR_CODE, NAME, -5.05, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void createItemNullArrivalDate() {
        itemService.createItem(VENDOR_CODE, NAME, PRICE, AVAILABILITY, null, PACKAGE_SIZE);
    }

    @Test
    public void createItemCorrectly() {
        String id = createSimpleItem();
        assertFalse(id.isEmpty());

        Item item = itemRepository.findOne(id);
        assertEquals(item.getVendorCode(), VENDOR_CODE);
        assertEquals(item.getName(), NAME);
        assertEquals(item.getPrice(), PRICE, 0.01);
        assertEquals(item.isAvailability(), AVAILABILITY);
        assertEquals(item.getArrivalDate(),ARRIVAL_DATE);
        assertEquals(item.getPackageSize(), PACKAGE_SIZE);
    }

    @Test
    public void createTwoItemsCorrectly() {
        List<String> ids = new ArrayList<>();
        String id0 = createSimpleItem();
        ids.add(id0);
        String id1 = createAnotherSimpleItem();
        ids.add(id1);

        List<Item> items = itemRepository.findAll();

        assertEquals(items.size(), 2);
        assertTrue(ids.contains(items.get(0).getId()));
        assertTrue(ids.contains(items.get(1).getId()));
    }

    //---EDIT ITEM TESTS------------------------------------------------------------------------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void editItemNullFields() {
        itemService.editItem(null, 0, null, 0.00, false, null, '-');
    }

    @Test(expected = ConstraintViolationException.class)
    public void editItemNegativeVendorCode() {
        itemService.editItem(RANDOM_UUID, -20, NAME, PRICE, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void editItemEmptyName() {
        itemService.editItem(RANDOM_UUID, VENDOR_CODE, "", PRICE, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void editItemNegativePrice() {
        itemService.editItem(RANDOM_UUID, VENDOR_CODE, NAME, -5.00, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    @Test(expected = ConstraintViolationException.class)
    public void editItemNullArrivalDate() {
        itemService.editItem(RANDOM_UUID, VENDOR_CODE, NAME, PRICE, AVAILABILITY, null, PACKAGE_SIZE);
    }

    @Test
    public void editItemNothingChanged() {
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), item.getName(), item.getPrice(),
                             item.isAvailability(), item.getArrivalDate(), item.getPackageSize());
    }

    @Test
    public void editItemVendorCodeChanged() {
        int newVendorCode = 33;
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, newVendorCode, item.getName(), item.getPrice(),
                item.isAvailability(), item.getArrivalDate(), item.getPackageSize());
        item = itemRepository.findOne(id);
        assertEquals(item.getVendorCode(), newVendorCode);
    }

    @Test
    public void editItemNameChanged() {
        String newName = "New name";
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), newName, item.getPrice(),
                item.isAvailability(), item.getArrivalDate(), item.getPackageSize());
        item = itemRepository.findOne(id);
        assertEquals(item.getName(), newName);
    }

    @Test
    public void editItemPriceChanged() {
        double newPrice = 37.99;
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), item.getName(), newPrice,
                item.isAvailability(), item.getArrivalDate(), item.getPackageSize());
        item = itemRepository.findOne(id);
        assertEquals(item.getPrice(), newPrice, 0.01);
    }

    @Test
    public void editItemAvailabilityChanged() {
        boolean newAvailability = !AVAILABILITY;
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), item.getName(), item.getPrice(),
                newAvailability, item.getArrivalDate(), item.getPackageSize());
        item = itemRepository.findOne(id);
        assertEquals(item.isAvailability(), newAvailability);
    }

    @Test
    public void editItemArrivalDateChanged() {
        Date newArrivalDate = new GregorianCalendar(2013, 3, 5).getTime();
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), item.getName(), item.getPrice(),
                item.isAvailability(), newArrivalDate, item.getPackageSize());
        item = itemRepository.findOne(id);
        assertEquals(item.getArrivalDate(), newArrivalDate);
    }

    @Test
    public void editItemPackageSizeChanged() {
        char newPackageSize = 'S';
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, item.getVendorCode(), item.getName(), item.getPrice(),
                item.isAvailability(), item.getArrivalDate(), newPackageSize);
        item = itemRepository.findOne(id);
        assertEquals(item.getPackageSize(), newPackageSize);
    }

    @Test
    public void editItemAllFieldsChanged() {
        String id = createSimpleItem();
        Item item = itemRepository.findOne(id);
        itemService.editItem(id, 78, "Pepper", 6.90, true, new GregorianCalendar(2011, 9, 15).getTime(), 'X');
        item = itemRepository.findOne(id);
        assertEquals(item.getVendorCode(), 78);
        assertEquals(item.getName(), "Pepper");
        assertEquals(item.getPrice(), 6.90, 0.01);
        assertEquals(item.isAvailability(), true);
        assertEquals(item.getArrivalDate(), new GregorianCalendar(2011, 9, 15).getTime());
        assertEquals(item.getPackageSize(), 'X');
    }

    //---REMOVE ITEM TESTS----------------------------------------------------------------------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void removeItemNullId() {
        itemService.removeItem(null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeItemNotExist() {
        itemService.removeItem(UUID.randomUUID().toString());
    }

    @Test
    public void removeItemCorrectly() {
        String id = createSimpleItem();
        assertNotNull(itemRepository.findOne(id));
        itemService.removeItem(id);
        assertNull(itemRepository.findOne(id));
    }

    //---FIND ONE ITEM TESTS--------------------------------------------------------------------------------------------

    @Test(expected = ConstraintViolationException.class)
    public void findOneNullId() {
        itemService.findOne(null);
    }

    @Test
    public void findOneCorrectly() {
        String id = createSimpleItem();
        ItemDto dto = itemService.findOne(id);
        Item item = itemRepository.findOne(id);

        assertEquals(dto.getId(), item.getId());
        assertEquals(dto.getVendorCode(), item.getVendorCode());
        assertEquals(dto.getName(), item.getName());
        assertEquals(dto.getPrice(), item.getPrice(), 0.01);
        assertEquals(dto.getArrivalDate(), DateParser.parseDateToString(item.getArrivalDate()));
        assertEquals(dto.getPackageSize(), item.getPackageSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void findOneItemNotExist() {
        createSimpleItem();
        itemService.findOne(UUID.randomUUID().toString());
    }

    //---FIND ALL ITEMS TESTS-------------------------------------------------------------------------------------------

    @Test
    public void findAllCorrectly() {
        List<String> ids = new ArrayList<>();
        String id0 = createSimpleItem();
        ids.add(id0);
        String id1 = createAnotherSimpleItem();
        ids.add(id1);
        List<ItemDto> dtos = itemService.findAll();
        assertEquals(dtos.size(), 2);
        assertTrue(ids.contains(dtos.get(0).getId()));
        assertTrue(ids.contains(dtos.get(1).getId()));
    }

    private String createSimpleItem() {
        return itemService.createItem(VENDOR_CODE, NAME, PRICE, AVAILABILITY, ARRIVAL_DATE, PACKAGE_SIZE);
    }

    private String createAnotherSimpleItem() {
        return itemService.createItem(202, "shoes", 35.00, true, new GregorianCalendar(2017, 1, 1).getTime(), '-');
    }

}
