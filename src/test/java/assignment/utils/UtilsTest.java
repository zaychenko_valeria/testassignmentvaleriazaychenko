package assignment.utils;


import assignment.domain.Item;
import assignment.dto.ItemDto;
import org.junit.Test;

import java.text.ParseException;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void parseDateFromStringCorrectly() throws ParseException {
        String dateStr = "2017-11-11";
        Date date = DateParser.parseStringToDate(dateStr);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        assertEquals(calendar.get(Calendar.YEAR), 2017);
        assertEquals(calendar.get(Calendar.MONTH) + 1, 11);
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 11);
    }

    @Test(expected = ParseException.class)
    public void parseDateFromString() throws ParseException {
        String dateStr = "2017/11/1";
        DateParser.parseStringToDate(dateStr);
    }

    @Test
    public void convertItemToDtoCorrectly() {
        Item item = new Item(22, "Random name", 3.11, true, new GregorianCalendar().getTime(), 'C');
        ItemDto dto = DtoBuilder.toDto(item);

        assertEquals(dto.getId(), item.getId());
        assertEquals(dto.getName(), item.getName());
        assertEquals(dto.getPrice(), item.getPrice(), 0.01);
        assertEquals(dto.getArrivalDate(), DateParser.parseDateToString(item.getArrivalDate()));
        assertEquals(dto.getVendorCode(), item.getVendorCode());
        assertEquals(dto.getPackageSize(), item.getPackageSize());
    }

    @Test
    public void convertItemCollectionToDtoCorrectly() {
        List<Item> items = new ArrayList<>();

        Item item0 = new Item(22, "Random name", 3.11, true, new GregorianCalendar().getTime(), 'C');
        Item item1 = new Item(66, "Another random name", 7.11, false, new GregorianCalendar().getTime(), 'M');
        items.add(item0);
        items.add(item1);

        List<ItemDto> dtos = DtoBuilder.collectionToDto(items.iterator());

        assertEquals(items.size(), dtos.size());
    }
}
