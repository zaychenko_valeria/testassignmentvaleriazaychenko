package assignment.controller;

import assignment.dto.ItemDto;
import assignment.service.IItemService;
import assignment.utils.DateParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ItemControllerTest {

    @Mock
    private IItemService mockItemService;
    @InjectMocks
    private ItemController theController;

    private MockMvc mockMvc;
    private String id;
    private ItemDto dto;
    private List<ItemDto> dtos;

    @Before
    public void setup() {
        dtos = new ArrayList<>();
        id = UUID.randomUUID().toString();

        dto = new ItemDto();
        dto.setId(id);
        dto.setVendorCode(35);
        dto.setName("Fish&Chips");
        dto.setPrice(8.21);
        dto.setAvailability(true);
        dto.setArrivalDate(DateParser.parseDateToString(new GregorianCalendar(2013, 11, 11).getTime()));
        dto.setPackageSize('M');

        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setSuffix(".jsp");

        this.mockMvc = MockMvcBuilders.standaloneSetup(theController)
                .setControllerAdvice(new ExceptionHandlingAdvice())
                .setViewResolvers(resolver)
                .build();
    }

    @Test
    public void viewAllItems() throws Exception {
        when(mockItemService.findAll()).thenReturn(dtos);

        this.mockMvc.perform(get("/items"))
                .andExpect(status().isOk())
                .andExpect(view().name("item"))
                .andExpect(model().attribute("items", dtos));

        verify(mockItemService, times(1))
                .findAll();
    }

    @Test
    public void getCreatePage() throws Exception {
        this.mockMvc.perform(get("/items/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("addOrEditItem"));
    }

    @Test
    public void createItem() throws Exception {
        this.mockMvc.perform(post("/items/add")
                        .param("vendorCode", String.valueOf(dto.getVendorCode()))
                        .param("name", dto.getName())
                        .param("price", String.valueOf(dto.getPrice()))
                        .param("availability", String.valueOf(dto.isAvailability()))
                        .param("arrivalDate", "2013-11-11")
                        .param("packageSize", String.valueOf(dto.getPackageSize())))
                .andExpect(status().isOk());

        verify(mockItemService, only())
                .createItem(
                        dto.getVendorCode(),
                        dto.getName(),
                        dto.getPrice(),
                        dto.isAvailability(),
                        DateParser.parseStringToDate("2013-11-11"),
                        dto.getPackageSize());
    }

    @Test
    public void getEditPage() throws Exception {
        when(mockItemService.findOne(id)).thenReturn(dto);

        this.mockMvc.perform(get("/items/edit/" + id)
                        .param("id", id))
                .andExpect(model().attribute("item", dto))
                .andExpect(view().name("addOrEditItem"));

        verify(mockItemService, only())
                .findOne(id);
    }

    @Test
    public void editItem() throws Exception {
        this.mockMvc.perform(
                post("/items/edit")
                        .param("id", id)
                        .param("vendorCode", String.valueOf(dto.getVendorCode()))
                        .param("name", dto.getName())
                        .param("price", String.valueOf(dto.getPrice()))
                        .param("availability", String.valueOf(dto.isAvailability()))
                        .param("arrivalDate", "2013-11-11")
                        .param("packageSize", String.valueOf(dto.getPackageSize())))
                .andExpect(status().isOk())
        ;

        verify(mockItemService, only())
                .editItem(
                        id,
                        dto.getVendorCode(),
                        dto.getName(),
                        dto.getPrice(),
                        dto.isAvailability(),
                        DateParser.parseStringToDate("2013-11-11"),
                        dto.getPackageSize());
    }

    @Test
    public void removeItem() throws Exception {
        this.mockMvc.perform(
                post("/items/deleteSelected")
                        .param("idsString", "[" + "\"" + id + "\"" + "]"))
                .andExpect(status().isOk());

        verify(mockItemService, only())
                .removeItem(id);
    }
}
