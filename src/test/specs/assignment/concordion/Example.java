package assignment.concordion;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class Example {

    public String greetingFor(String firstName) {
        return "Hello " + firstName + "!";
    }

    public String availableWithoutArrivalDate(String isAvailable) {
        return "MSG_1";
    }
}
